import { Component } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  id: number;
  caudal: any;
  nivel: any;

  constructor(public http: Http) {
    this.request();
  }

  request() {
    if (this.id) clearTimeout(this.id);

    this.http.get('http://estacionclimatica.pw/api.php')
    .map(res => res.json())
    .subscribe(data => {
      console.log(data);
      this.caudal = data.caudal;
      this.nivel = data.nivel;
      this.id = setTimeout(() => this.request(), 1000);
    });
  }

}
